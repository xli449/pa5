#include "List.h"
#include <math.h>
#include <map>
#include <unordered_map>
#include <string>
List::List() {
	this->num_elements = 0;
	this->cursor = NULL;
	this->afterCursor = NULL;
	this->backDummy = NULL;
	this->beforeCursor = NULL;
	this->frontDummy = NULL;
}

List::Node::Node(ListElement x) {
	this->data = x;
	this->prev = NULL;
	this->next = NULL;
}

int List::length() const {
	return num_elements;
}
List::List(const List& L) {
	this->num_elements = 0;
	this->cursor = NULL;
	this->afterCursor = NULL;
	this->backDummy = NULL;
	this->beforeCursor = NULL;
	this->frontDummy = NULL;
	Node* tmp = L.frontDummy;
	int numk = L.length();
	while (numk--) {
		++num_elements;
		if (this->frontDummy == NULL) {
			Node* p = new List::Node(tmp->data);
			frontDummy = p;
			backDummy = p;
			cursor = p;
			pos_cursor = 0;
		}
		else {
			Node* p=new List::Node(tmp->data);
			cursor->next = p;
			p->prev = cursor;
			cursor = p;
			backDummy = p;
			pos_cursor++;
		}
		tmp = tmp->next;
	}
}

ListElement List::front()const {
	if (frontDummy == NULL) return -1;
	return frontDummy->data;
}

int List::position() const {
	return pos_cursor;
}

ListElement List::back()const {
	if (backDummy == NULL) return -1;
	return backDummy->data;
}

 List::~List() {

}

 ListElement List::peekNext() const {
	 if (cursor->next != NULL) return cursor->next->data;
	 return -1;
 }

 ListElement List::peekPrev() const {
	 if (cursor->prev != NULL) return cursor->prev->data;
	 return -1;
 }

 void List::clear() {
	 this->num_elements = 0;
	 Node* tmp = this->frontDummy;
	 while (tmp != NULL) {
		 Node* p = tmp;
		 tmp = tmp->next;
		 free(p);
	 }
	 this->cursor = NULL;
	 this->afterCursor = NULL;
	 this->backDummy = NULL;
	 this->beforeCursor = NULL;
	 this->frontDummy = NULL;
 }

 void List::moveFront() {
	 if (num_elements == 0) return;
	 pos_cursor = 0;
	 cursor = frontDummy;
 }

 void List::moveBack() {
	 if (num_elements == 0) return;
	 pos_cursor = num_elements-1;
	 cursor = backDummy;
 }

 ListElement List::moveNext() {
	 if (cursor == NULL) return -1;
	 if (pos_cursor == num_elements - 1) return cursor->data;
	 int hk = cursor->data;
	 cursor = cursor->next;
	 return hk;
 }

 ListElement List::movePrev() {
	 if (cursor == NULL) return -1;
	 if (pos_cursor == 0) return cursor->data;
	 int hk = cursor->data;
	 cursor = cursor->prev;
	 return hk;
 }

 void List::insertAfter(ListElement x) {
	 Node* p = new List::Node(x);
	 num_elements++;
	 if (cursor == NULL) {
		 cursor = p;
		 frontDummy = p;
		 backDummy = p;
		 pos_cursor = 0;
	 }
	 else {
		 p->next = cursor->next;
		 p->prev = cursor;
		 cursor->next = p;
		 if (p->next != NULL) {
			 p->next->prev = p;
		 }
		 else {
			 backDummy = p;
		 }
		 pos_cursor += 1;
	 }
	 cursor = p;
 }

 void List::insertBefore(ListElement x) {
	 Node* p = new List::Node(x);
	 num_elements++;
	 if (cursor == NULL) {
		 cursor = p;
		 frontDummy = p;
		 backDummy = p;
		 pos_cursor = 0;
	 }
	 else {
		 p->next = cursor;
		 p->prev = cursor->prev;
		 cursor->prev = p;
		 if (p->prev != NULL) {
			 p->prev->next = p;
		 }
		 else {
			 frontDummy = p;
		 }
	 }
	 cursor = p;
 }

 void List::setAfter(ListElement x) {
	 if (cursor->next!=NULL) {
		 cursor->next->data = x;
	 }
 }


 void List::setBefore(ListElement x) {
	 if (cursor->prev != NULL) {
		 cursor->prev->data = x;
	 }
 }

 std::ostream& operator<<(std::ostream& stream, const List& L) {
	 List::Node* tmp = L.frontDummy;
	 while (tmp != NULL) {
		 stream << tmp->data << " ";
		 tmp = tmp->next;
	 }
	 stream << std::endl;
	 return stream;
 }

 bool operator==(const List& A, const List& B) {
	 if (A.num_elements != B.num_elements) return false;
	 int numk = A.num_elements;
	 List::Node* tmpA = A.frontDummy;
	 List::Node* tmpB = B.frontDummy;
	 while (numk--) {
		 if (tmpA->data != tmpB->data) return false;
		 tmpA = tmpA->next;
		 tmpB = tmpB->next;
	 }
	 return true;
 }

 int List::findNext(ListElement x) {
	 if (cursor == NULL) return -1;
	 cursor = cursor->next;
	 int vis = 0;
	 while (cursor != NULL) {
		 ++pos_cursor;
		 if (cursor->data == x) {
			 vis = 1;
			 break;
		 }
		 cursor = cursor->next;
	 }
	 if (vis) {
		 cursor = cursor->next;
		 ++pos_cursor;
		 return pos_cursor;
	 }
	 else {
		 return -1;
	 }
 }


 int List::findPrev(ListElement x) {
	 if (cursor == NULL) return -1;
	 cursor = cursor->prev;
	 int vis = 0;
	 while (cursor != NULL) {
		 --pos_cursor;
		 if (cursor->data == x) {
			 vis = 1;
			 break;
		 }
		 cursor = cursor->prev;
	 }
	 if (vis) {
		 cursor = cursor->prev;
		 --pos_cursor;
		 return pos_cursor;
	 }
	 else {
		 return -1;
	 }
 }

 void List::eraseAfter() {
	 if (cursor == NULL) return;
	 Node* tmp = cursor->next;
	 
	 if (tmp == NULL) {
		 return;
	 }
	 else {
		 cursor->next = tmp->next;
		 if(tmp->next!=NULL) tmp->next->prev = cursor;
		 else {
			 backDummy = cursor;
		 }
		 free(tmp);
		 num_elements--;
	 }
 }

 void List::eraseBefore() {
	 if (cursor == NULL) return;
	 Node* tmp = cursor->prev;

	 if (tmp == NULL) {
		 return;
	 }
	 else {
		 cursor->prev = tmp->prev;
		 if (tmp->prev != NULL) tmp->prev->next = cursor;
		 else {
			 frontDummy = cursor;
		 }
		 free(tmp);
		 num_elements--;
	 }
 }

 void List::cleanup() {
	 std::unordered_map<int, int> mp;
	 Node* tmp = frontDummy;
	 mp[tmp->data] = 1;
	 Node* pre = tmp;
	 tmp = tmp->next;
	 while (tmp != NULL) {
		 if (mp.find(tmp->data) != mp.end()) {
			 Node* g = tmp;
			 tmp = tmp->next;
			 pre->next = tmp;
			 if (tmp != NULL) {
				 tmp->prev = pre;
			 }
			 free(g);
			 num_elements--;
		 }
		 else {
			 mp[tmp->data] = 1;
			 pre = tmp;
			 tmp = tmp->next;
		 }
	 }
	 backDummy = pre;
 }

 List List::concat(const List& L) const {
	 List G;
	 G.num_elements = num_elements + L.num_elements;
	 Node* tmp = frontDummy;
	 while (tmp != NULL) {
		 if (G.cursor == NULL) {
			 Node* p = new List::Node(tmp->data);
			 G.frontDummy = p;
			 G.backDummy = p;
			 G.cursor = p;
		 }
		 else {
			 Node* p = new List::Node(tmp->data);
			 G.cursor->next = p;
			 p->prev = G.cursor;
			 G.cursor = p;
			 G.backDummy = cursor;
		 }
		 tmp = tmp->next;
	 }
	 tmp = L.frontDummy;
	 while (tmp != NULL) {
		 if (G.cursor == NULL) {
			 Node* p = new List::Node(tmp->data);
			 G.frontDummy = p;
			 G.backDummy = p;
			 G.cursor = p;
		 }
		 else {
			 Node* p = new List::Node(tmp->data);
			 G.cursor->next = p;
			 p->prev = G.cursor;
			 G.cursor = p;
			 G.backDummy = cursor;
		 }
		 tmp = tmp->next;
	 }
	 G.cursor = G.frontDummy;
	 return G;
 }

 std::string List::to_string() const {
	 std::string s = "";
	 Node* tmp = frontDummy;
	 while (tmp != NULL) {
		 s += std::to_string(tmp->data);
		 tmp = tmp->next;
	 }
	 return s;
 }

 List::Node* List::frontNode() {
	 return this->frontDummy;
 }
 List::Node* List::backNode() {
	 return backDummy;
 }
