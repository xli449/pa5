#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include "List.h"

using namespace std;
int main(int argc, char* argv[])
{
    FILE* fp = fopen("out", "w");
    if (fp == NULL)
    {
        return 0;
    }
    List s, t;
    if (argc != 2) {
        printf("Wrong number of parameters!");
        return 0;
    }
    int  n = atoi(argv[1]);
    for (int i = 1; i <= n; ++i) {
        s.clear();
        t.clear();
        for (int j = 1; j <= i; ++j) {
            s.insertAfter(j);
            t.insertAfter(j);
        }
        int numk = 1;
        int gnum = 0;
        while (true) {
            ++gnum;
            int num1 = i / 2;
            int num2 = i - num1;
            List tmp1;
            List tmp2;
            t.moveFront();
            for (int k = 1; k <= num1; ++k) {
                tmp1.insertAfter(t.moveNext());
            }
            for (int k = 1; k <= num2; ++k) {
                tmp2.insertAfter(t.moveNext());
            }
            t.clear();
            tmp1.moveFront();
            tmp2.moveFront();
            for (int k = 1; k <= num2; ++k) {
                t.insertAfter(tmp2.moveNext());
                if (k <= num1) t.insertAfter(tmp1.moveNext());
            }
            if (t == s) {
                break;
            }
        }
        fprintf(fp,"%d    %d\n", i, gnum);
    }
    fclose(fp);

}
